import { DataTypes } from "sequelize";
import db from '../../db/connection';

const User=db.define("User",{
    name:{
        type:DataTypes.STRING
    },
    last_name:{
        type:DataTypes.STRING
    },
    num_doc:{
        type:DataTypes.INTEGER
    },
    tip_doc:{
        type:DataTypes.INTEGER
    },
    age:{
        type:DataTypes.INTEGER
    },
    city:{
        type:DataTypes.INTEGER
    }

});

export default User;