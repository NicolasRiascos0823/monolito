import { Schema, model } from "mongoose";

const UserSchema=new Schema({
    name: {
        type: String,
        required: [true, 'The name is obligatory']
    },
    last_name: {
        type: String,
        required: [true, 'The last name is obligatory']
    },
    num_doc: {
        type: Number,
        required: [true, 'The num_doc is obligatory']
    },
    tip_doc:{
        type:Number,
        required: [true,'The Type of document is obligatory']
    },
    age:{
        type:Number,
        required:[true,'The age is obligatory']
    },
    city:{
        type:Number,
        required:[true,'The city is obligatory']
    }
});
export = model('User',UserSchema);