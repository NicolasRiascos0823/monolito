import { Schema, model } from "mongoose";

const ImageSchema=new Schema({
    image: {
        type: String,
        required: [true, 'The image is obligatory']
    },
    num_doc: {
        type: Number,
        required: [true, 'The num_doc is obligatory']
    }
});
export = model('Image',ImageSchema);