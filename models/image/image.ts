import { DataTypes } from "sequelize";
import db from '../../db/connection';

const Image=db.define("Image",{
    image:{
        type:DataTypes.TEXT
    },
    user_id:{
        type:DataTypes.INTEGER
    }
})

export default Image;