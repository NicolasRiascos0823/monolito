import express,{Application} from 'express';
import router from '../routes/user/user';
import routerUserMongo from '../routes/user/userMongo';
import routerImg from '../routes/image/images';
import routerImgMongo from '../routes/image/imagesMongo';
import cors  from 'cors';
import db from '../db/connection';
import { dbConnect } from '../db/connectionMongo';

class Server{

    private ENGINE_DB=process.env.ENGINE_DB;
    private app: Application;
    private port:string;
    private apiPaths={
        users:'/api/users',
        images:'/api/images'
    }

    constructor(){
        this.app=express();
        this.port=process.env.PORT || '8000';

        if (this.ENGINE_DB==='mysql') {
            //BD Connection MYSQL
            this.db_connect();
        }else if(this.ENGINE_DB==='mongo'){
            //BD Connection MONGO
            this.db_connect_mongo();
        }
        
        
        //initial methods
        this.middlewares();
        //routes definition
        this.routes();
    }
    //BD Connection
    async db_connect(){
        try {
            await db.authenticate();
            //console.log('Database is ok');
            
        } catch (error) {
            throw new Error(error as any);
        }
    }
    async db_connect_mongo(){
        await dbConnect();
    }

    middlewares(){
        //CORS
        this.app.use(cors());
        //reading of body
        this.app.use(express.json());
        //Folder public
        this.app.use(express.static('public'));

       
    }
    routes(){
        if (this.ENGINE_DB==='mysql') {
            this.app.use(this.apiPaths.users,router);
            this.app.use(this.apiPaths.images,routerImg);
        }else if(this.ENGINE_DB==='mongo'){
            this.app.use(this.apiPaths.users,routerUserMongo);
            this.app.use(this.apiPaths.images,routerImgMongo);
        }
        
    }
    listen(){
        this.app.listen(this.port,()=>{
            console.log('Server running in port: '+this.port);
        })
    }
}
export default Server;