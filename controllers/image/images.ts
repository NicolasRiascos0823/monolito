import e, { Request } from "express";
import { Response } from "express";
import Image from "../../models/image/image";
import User from "../../models/user/user";

export const getImages=async(req:Request, res:Response)=>{
    const images=await Image.findAll();

    res.json({images});
}

export const getImage=async(req:Request, res:Response)=>{
    const {user_id}=req.params;

    try {
        const userImg=await Image.findOne({
            where:{user_id}
        })
        if(!userImg){
            return res.status(400).json({
                msg:`The image of user ID: ${user_id} not already registered`
            })
        }
        res.json({userImg});
    } catch (error) {
        console.log(error);
        res.status(400).json({
            msg:"The image not found"
        })
    }
}


export const uploadImage=async(req:Request, res:Response)=>{
    
    const {body}=req;

    //console.log(req.files);

    const base64 = req.file?.buffer.toString('base64');
    try {   

        //const image=new Image(body);
        //image.save();
        const existUser= await User.findOne({
            where:{
                id: body.user_id
            }
        });
        if(!existUser){
            return res.status(400).json({
                msg:`The user is not already exist with ID: ${body.user_id}`
            })
        }
        const existUserImg=await Image.findOne({
            where:{
                user_id:body.user_id
            }
        })
        if(existUserImg){
            return res.status(400).json({
                msg:`The user ID: ${body.user_id} has a image registered`
            })
        }else{
            const image=await Image.create({
                id:body.id,
                image:base64,
                user_id:body.user_id
            });
        }
        
        //res.json({base64, user: body.user_id});

        res.json({
            msg:`The image of user ID: ${body.user_id} has been registered`
        });
        
    } catch (error) {
        console.log(error);
        res.status(500).json("You have a error in the data");
    }
    
   
}
export const putImage=async(req:Request,res:Response)=>{
    const {user_id}=req.params;
    const {body}=req;
    const base64 = req.file?.buffer.toString('base64');
    try {
        const user= await User.findByPk(user_id);
        if (!user) {
            return res.status(404).json({
                msg:"The user not already exist ID: "+user_id
            })
        }
        const userImg=await Image.findOne({
            where:{user_id}
        })
        if(!userImg){
            return res.status(400).json({
                msg:`The image of user ID: ${user_id} not already registered`
            })
        }
        if(!base64){
            return res.status(400).json({
                msg:`No image was loaded`
            })
        }
        await userImg.update({
            image:base64
        });
        res.json({userImg});

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:"You have a error in the data"
        })
        
    }
}
export const deleteImg=async(req:Request,res:Response)=>{
    const {user_id}=req.params;
    try {
        const userImg=await Image.findOne({
            where:{user_id}
        })
        if(!userImg){
            return res.status(400).json({
                msg:`The image of user ID: ${user_id} not already registered`
            })
        }
        userImg.destroy();

        res.json({
            msg:`The image of user ID: ${user_id} has been deleted`
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:"You have a error in the data"
        })
    }
}