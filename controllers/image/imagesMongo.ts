import { Request } from "express";
import { Response } from "express";
import Image from "../../models/image/imageMongo";
import User from "../../models/user/userMongo";

export const getImages=async (req:Request,res:Response)=>{
    
    const {body}=req;
    const {id}=req.params;

    try {
        const images=await Image.findOne({
            where:{
                _id: id
            }
        });

        res.json(images);
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }
}
export const getImage=async (req:Request,res:Response)=>{

    const {id}=req.params;
    const image=await Image.findById(id);

    try {
       
        if (!image) {
            return res.status(404).json({
                msg:`The user haven't a image registered`
            });
        }        
        res.json(image);

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }

}

export const postImage=async (req:Request,res:Response)=>{

    const {body}=req;

    const base64=req.file?.buffer.toString('base64');

    try {

        const user=await User.findOne(body,{
            where:{
                num_doc:body.num_doc
            }
        });

        if (!user) {
            return res.json({
                msg:`The user ${body.num_doc} is not already exist`
            })
        }

        const image=await Image.findOne(body,{
            where:{
                num_doc:body.num_doc
            }
        });
        if (image) {
            return res.json({
                msg:`The user ${body.num_doc} has a image registered`
            })
        }
        const upload=await Image.create({
            image:base64,
            num_doc:body.num_doc
        });

        res.json({
            msg:`The image of user ${body.num_doc} has been registered succesfully`
        });
        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }

}
export const putImage=async (req:Request,res:Response)=>{

    const {body}=req;
    const {id}=req.params;
    const base64=req.file?.buffer.toString('base64');

    const image=await Image.findById(id);

    try {

        if(!image){
            return res.json({
                msg:`The user with ID ${id} haven't a image registered`
            })
        }
        
        const update=await Image.findByIdAndUpdate(id,body,{new:true});

        res.json({
            msg:`The image of user ID: ${id} has been updeated`,
            update
        })
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }
}
export const deleteImage=async (req:Request,res:Response)=>{

    const {id}=req.params;

    const image=await Image.findById(id);

    try {
        if(!image){
            return res.json({
                msg:`The image of user with ID ${id} haven't a image registered`
            })
        }
        await Image.findByIdAndDelete(id);

        res.json("The image has been deleted successfully");
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }
}