import { Request } from "express";
import { Response } from "express";
import User from "../../models/user/user";



export const getUsers=async(req:Request, res:Response)=>{

    const users= await User.findAll();

    res.json({users});

}
export const getUser= async(req: Request, res:Response)=>{
    const {id}=req.params; 

    /* res.json({
        msg:'getUser'
    }) */
    const user= await User.findOne({
        where:{id}
    });
    try {
        if (!user) {
            return res.status(400).json({
                msg:`The user with ID: ${id} not already exist`
            })
        }

        res.json({user});


    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }
}
export const postUser= async (req:Request, res:Response)=>{
    const body=req.body;

    /* res.json({
        msg:'postUser',
        body
    }) */
    try {

        const existId= await User.findOne({
            where:{
                id: body.id
            }
        });

        if (existId) {
            return res.status(400).json({
                msg:'The user is already exist with ID: '+body.id+" name: "+body.name
            })
        }

        const user=new User(body);
        await user.save();

        res.json({
            msg:`The user ${body.name} has been registered successfully`,
            user
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }
}
export const putUser=async(req:Request, res:Response)=>{
    const {body}=req;
    const {id}=req.params;

    /* res.json({
        msg:'putUser',
        body,
        id
    }) */
    try {
        
        const user= await User.findByPk(id);
        if (!user) {
            return res.status(404).json({
                msg:"The user not already exist ID: "+id
            })
        }
        await user.update(body);

        res.json({
            msg:`The user ${body.name} it has updated successfully`,
            user
        });

    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }
}
export const deleteUser=async(req:Request, res:Response)=>{
    const {id}=req.params;

    /* res.json({
        msg:'deleteUser',
        id
    }) */
    const user= await User.findByPk(id);
    if (!user) {
        return res.status(404).json({
            msg:"The user not already exist ID: "+id
        })
    }

    //logic delete
    //await user.update({status:false});


    //physic delete
    await user.destroy();

    res.json({
        msg:`The user with ID:${id}  has been deleted`
    })
}
