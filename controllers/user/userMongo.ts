import { Request } from "express";
import { Response } from "express";
import User from '../../models/user/userMongo'


export const getUsers=async(req:Request,res:Response)=>{
    
    try {
        const user= await User.find();
        res.json(user);
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }
}
export const getUser=async(req:Request,res:Response)=>{
    const {body}=req;
    const {id}=req.params;
    const user= await User.findById(id);

    try {
        if (!user) {
            //throw new Error(`The user with ID: ${num_doc} is not already registered`);
            return res.status(400).json({
                msg:`The user with ID: ${id} is not already registered`
            });
        }
        res.json({user});
    } catch (error) {
        //next(error);
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }

}
export const postUser=async(req:Request,res:Response)=>{
    const {body}=req;

    try {
       
        const validate=await User.findOne(body,{
            where:{
                num_doc:body.num_doc
            }
        });

        if (validate) {
            return res.json({
                msg:`The user ${body.name} with num_doc: ${body.num_doc} is already exist`
            });
        }
        const user=new User(body);
        await user.save();

        res.json({
            msg:`The user ${body.name} has been registered successfully`,
            user
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }

}
export const putUser=async(req:Request,res:Response)=>{
    const {body}=req;
    const {id}=req.params;

    const user= await User.findOne(body,{
        where:{
            _id:id
        }
    });
    try {
        if (!user) {
            //throw new Error(`The user with ID: ${num_doc} is not already registered`);
            return res.status(400).json({
                msg:`The user with ID: ${body.name} ${body.last_name} is not already registered`
            });
        }
        await user.updateOne(body);

        res.json({
            msg:`The user ${body.name} ${body.last_name} it has updated successfully`,
            user
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }
}
export const deleteUser=async(req:Request,res:Response)=>{

    const {body}=req;

    const {id}=req.params;

    try {
        const validate=await User.findOne(body,{
            where:{
                num_doc:body.num_doc
            }
        });
        if (!validate) {
            return res.json({
                msg:`The user ${body.name} with num_doc: ${body.num_doc} is not already exist`
            });
        }

        await User.deleteOne(body);
        res.json({
            msg:`The user ${body.name} has been deleted`
        })


    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg:'You have a error in the data'
        });
    }

}