import mongoose from "mongoose";



export const dbConnect=async()=>{

    

    try {

        mongoose.connect(process.env.MONGODB_CNN, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        //console.log("Database Connected");
        
    } catch (error) {
        console.log(error)
        throw new Error("Error to connect BD MONGO");
        
    }
    
}