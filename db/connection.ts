import { Sequelize } from "sequelize";

const db=new Sequelize('test_node','root','',{
    host:'localhost',
    dialect: 'mysql',
    logging: false,
});

export default db;