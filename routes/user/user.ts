import { Router } from "express";
import { deleteUser, getUsers, postUser, putUser,getUser } from "../../controllers/user/user";

const router=Router();

    router.get('/',getUsers);
    router.get('/:id',getUser);
    router.post('/',postUser);
    router.put('/:id',putUser);
    router.delete('/:id',deleteUser);

export default router;