import { Router } from "express";
import {  uploadImage, getImages, getImage,putImage, deleteImg } from "../../controllers/image/images";
//import { storage } from "../config/multer";
import multer from "multer";

const uploader = multer();


const route=Router();

    route.get('/',getImages);
    route.get('/:user_id',getImage);
    route.post('/',uploader.single('image'),uploadImage);
    route.put('/:user_id',uploader.single('image'),putImage);
    route.delete('/:user_id',deleteImg);



export default route;