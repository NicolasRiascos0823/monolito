import { Router } from "express";
import { deleteImage, getImage, getImages, postImage, putImage } from "../../controllers/image/imagesMongo";
//import { storage } from "../config/multer";
import multer from "multer";

const uploader = multer();

const route=Router();

route.get('/',getImages);
route.get('/:id',getImage);
route.post('/',uploader.single('image'),postImage);
route.put('/:id',uploader.single('image'),putImage);
route.delete('/:id',deleteImage);

export default route;