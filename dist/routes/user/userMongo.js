"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const userMongo_1 = require("../../controllers/user/userMongo");
const route = (0, express_1.Router)();
route.get('/', userMongo_1.getUsers);
route.get('/:id', userMongo_1.getUser);
route.post('/', userMongo_1.postUser);
route.put('/:id', userMongo_1.putUser);
route.delete('/:id', userMongo_1.deleteUser);
exports.default = route;
//# sourceMappingURL=userMongo.js.map