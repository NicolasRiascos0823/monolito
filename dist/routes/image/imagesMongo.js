"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const imagesMongo_1 = require("../../controllers/image/imagesMongo");
//import { storage } from "../config/multer";
const multer_1 = __importDefault(require("multer"));
const uploader = (0, multer_1.default)();
const route = (0, express_1.Router)();
route.get('/', imagesMongo_1.getImages);
route.get('/:id', imagesMongo_1.getImage);
route.post('/', uploader.single('image'), imagesMongo_1.postImage);
route.put('/:id', uploader.single('image'), imagesMongo_1.putImage);
route.delete('/:id', imagesMongo_1.deleteImage);
exports.default = route;
//# sourceMappingURL=imagesMongo.js.map