"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const images_1 = require("../controllers/image/images");
//import { storage } from "../config/multer";
const multer_1 = __importDefault(require("multer"));
const uploader = (0, multer_1.default)();
const route = (0, express_1.Router)();
if (process.env.ENGINE_BD == "mysql") {
    route.get('/', images_1.getImages);
    route.get('/:user_id', images_1.getImage);
    route.post('/', uploader.single('image'), images_1.uploadImage);
    route.put('/:user_id', uploader.single('image'), images_1.putImage);
    route.delete('/:user_id', images_1.deleteImg);
}
else if (process.env.ENGINE_BD === "mongo") {
    route.get('/', images_1.getImages);
}
exports.default = route;
//# sourceMappingURL=images.js.map