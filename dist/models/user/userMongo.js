"use strict";
const mongoose_1 = require("mongoose");
const UserSchema = new mongoose_1.Schema({
    name: {
        type: String,
        required: [true, 'The name is obligatory']
    },
    last_name: {
        type: String,
        required: [true, 'The last name is obligatory']
    },
    num_doc: {
        type: Number,
        required: [true, 'The num_doc is obligatory']
    },
    tip_doc: {
        type: Number,
        required: [true, 'The Type of document is obligatory']
    },
    age: {
        type: Number,
        required: [true, 'The age is obligatory']
    },
    city: {
        type: Number,
        required: [true, 'The city is obligatory']
    }
});
module.exports = (0, mongoose_1.model)('User', UserSchema);
//# sourceMappingURL=userMongo.js.map