"use strict";
const mongoose_1 = require("mongoose");
const ImageSchema = new mongoose_1.Schema({
    image: {
        type: String,
        required: [true, 'The image is obligatory']
    },
    num_doc: {
        type: Number,
        required: [true, 'The num_doc is obligatory']
    }
});
module.exports = (0, mongoose_1.model)('Image', ImageSchema);
//# sourceMappingURL=imageMongo.js.map