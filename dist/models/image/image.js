"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const connection_1 = __importDefault(require("../../db/connection"));
const Image = connection_1.default.define("Image", {
    image: {
        type: sequelize_1.DataTypes.TEXT
    },
    user_id: {
        type: sequelize_1.DataTypes.INTEGER
    }
});
exports.default = Image;
//# sourceMappingURL=image.js.map