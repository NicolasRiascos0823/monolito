"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const user_1 = __importDefault(require("../routes/user/user"));
const userMongo_1 = __importDefault(require("../routes/user/userMongo"));
const images_1 = __importDefault(require("../routes/image/images"));
const imagesMongo_1 = __importDefault(require("../routes/image/imagesMongo"));
const cors_1 = __importDefault(require("cors"));
const connection_1 = __importDefault(require("../db/connection"));
const connectionMongo_1 = require("../db/connectionMongo");
class Server {
    constructor() {
        this.ENGINE_DB = process.env.ENGINE_DB;
        this.apiPaths = {
            users: '/api/users',
            images: '/api/images'
        };
        this.app = (0, express_1.default)();
        this.port = process.env.PORT || '8000';
        if (this.ENGINE_DB === 'mysql') {
            //BD Connection MYSQL
            this.db_connect();
        }
        else if (this.ENGINE_DB === 'mongo') {
            //BD Connection MONGO
            this.db_connect_mongo();
        }
        //initial methods
        this.middlewares();
        //routes definition
        this.routes();
    }
    //BD Connection
    db_connect() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield connection_1.default.authenticate();
                //console.log('Database is ok');
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    db_connect_mongo() {
        return __awaiter(this, void 0, void 0, function* () {
            yield (0, connectionMongo_1.dbConnect)();
        });
    }
    middlewares() {
        //CORS
        this.app.use((0, cors_1.default)());
        //reading of body
        this.app.use(express_1.default.json());
        //Folder public
        this.app.use(express_1.default.static('public'));
    }
    routes() {
        if (this.ENGINE_DB === 'mysql') {
            this.app.use(this.apiPaths.users, user_1.default);
            this.app.use(this.apiPaths.images, images_1.default);
        }
        else if (this.ENGINE_DB === 'mongo') {
            this.app.use(this.apiPaths.users, userMongo_1.default);
            this.app.use(this.apiPaths.images, imagesMongo_1.default);
        }
    }
    listen() {
        this.app.listen(this.port, () => {
            console.log('Server running in port: ' + this.port);
        });
    }
}
exports.default = Server;
//# sourceMappingURL=server.js.map