"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteImg = exports.putImage = exports.uploadImage = exports.getImage = exports.getImages = void 0;
const image_1 = __importDefault(require("../../models/image/image"));
const user_1 = __importDefault(require("../../models/user/user"));
const getImages = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const images = yield image_1.default.findAll();
    res.json({ images });
});
exports.getImages = getImages;
const getImage = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { user_id } = req.params;
    try {
        const userImg = yield image_1.default.findOne({
            where: { user_id }
        });
        if (!userImg) {
            return res.status(400).json({
                msg: `The image of user ID: ${user_id} not already registered`
            });
        }
        res.json({ userImg });
    }
    catch (error) {
        console.log(error);
        res.status(400).json({
            msg: "The image not found"
        });
    }
});
exports.getImage = getImage;
const uploadImage = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const { body } = req;
    //console.log(req.files);
    const base64 = (_a = req.file) === null || _a === void 0 ? void 0 : _a.buffer.toString('base64');
    try {
        //const image=new Image(body);
        //image.save();
        const existUser = yield user_1.default.findOne({
            where: {
                id: body.user_id
            }
        });
        if (!existUser) {
            return res.status(400).json({
                msg: `The user is not already exist with ID: ${body.user_id}`
            });
        }
        const existUserImg = yield image_1.default.findOne({
            where: {
                user_id: body.user_id
            }
        });
        if (existUserImg) {
            return res.status(400).json({
                msg: `The user ID: ${body.user_id} has a image registered`
            });
        }
        else {
            const image = yield image_1.default.create({
                id: body.id,
                image: base64,
                user_id: body.user_id
            });
        }
        //res.json({base64, user: body.user_id});
        res.json({
            msg: `The image of user ID: ${body.user_id} has been registered`
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json("You have a error in the data");
    }
});
exports.uploadImage = uploadImage;
const putImage = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    const { user_id } = req.params;
    const { body } = req;
    const base64 = (_b = req.file) === null || _b === void 0 ? void 0 : _b.buffer.toString('base64');
    try {
        const user = yield user_1.default.findByPk(user_id);
        if (!user) {
            return res.status(404).json({
                msg: "The user not already exist ID: " + user_id
            });
        }
        const userImg = yield image_1.default.findOne({
            where: { user_id }
        });
        if (!userImg) {
            return res.status(400).json({
                msg: `The image of user ID: ${user_id} not already registered`
            });
        }
        if (!base64) {
            return res.status(400).json({
                msg: `No image was loaded`
            });
        }
        yield userImg.update({
            image: base64
        });
        res.json({ userImg });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "You have a error in the data"
        });
    }
});
exports.putImage = putImage;
const deleteImg = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { user_id } = req.params;
    try {
        const userImg = yield image_1.default.findOne({
            where: { user_id }
        });
        if (!userImg) {
            return res.status(400).json({
                msg: `The image of user ID: ${user_id} not already registered`
            });
        }
        userImg.destroy();
        res.json({
            msg: `The image of user ID: ${user_id} has been deleted`
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "You have a error in the data"
        });
    }
});
exports.deleteImg = deleteImg;
//# sourceMappingURL=images.js.map