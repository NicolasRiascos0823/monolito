"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteImage = exports.putImage = exports.postImage = exports.getImage = exports.getImages = void 0;
const imageMongo_1 = __importDefault(require("../../models/image/imageMongo"));
const userMongo_1 = __importDefault(require("../../models/user/userMongo"));
const getImages = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    const { id } = req.params;
    try {
        const images = yield imageMongo_1.default.findOne({
            where: {
                _id: id
            }
        });
        res.json(images);
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.getImages = getImages;
const getImage = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const image = yield imageMongo_1.default.findById(id);
    try {
        if (!image) {
            return res.status(404).json({
                msg: `The user haven't a image registered`
            });
        }
        res.json(image);
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.getImage = getImage;
const postImage = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    const { body } = req;
    const base64 = (_a = req.file) === null || _a === void 0 ? void 0 : _a.buffer.toString('base64');
    try {
        const user = yield userMongo_1.default.findOne(body, {
            where: {
                num_doc: body.num_doc
            }
        });
        if (!user) {
            return res.json({
                msg: `The user ${body.num_doc} is not already exist`
            });
        }
        const image = yield imageMongo_1.default.findOne(body, {
            where: {
                num_doc: body.num_doc
            }
        });
        if (image) {
            return res.json({
                msg: `The user ${body.num_doc} has a image registered`
            });
        }
        const upload = yield imageMongo_1.default.create({
            image: base64,
            num_doc: body.num_doc
        });
        res.json({
            msg: `The image of user ${body.num_doc} has been registered succesfully`
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.postImage = postImage;
const putImage = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    const { body } = req;
    const { id } = req.params;
    const base64 = (_b = req.file) === null || _b === void 0 ? void 0 : _b.buffer.toString('base64');
    const image = yield imageMongo_1.default.findById(id);
    try {
        if (!image) {
            return res.json({
                msg: `The user with ID ${id} haven't a image registered`
            });
        }
        const update = yield imageMongo_1.default.findByIdAndUpdate(id, body, { new: true });
        res.json({
            msg: `The image of user ID: ${id} has been updeated`,
            update
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.putImage = putImage;
const deleteImage = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const image = yield imageMongo_1.default.findById(id);
    try {
        if (!image) {
            return res.json({
                msg: `The image of user with ID ${id} haven't a image registered`
            });
        }
        yield imageMongo_1.default.findByIdAndDelete(id);
        res.json("The image has been deleted successfully");
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.deleteImage = deleteImage;
//# sourceMappingURL=imagesMongo.js.map