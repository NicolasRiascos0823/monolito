"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUser = exports.putUser = exports.postUser = exports.getUser = exports.getUsers = void 0;
const userMongo_1 = __importDefault(require("../../models/user/userMongo"));
const getUsers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const user = yield userMongo_1.default.find();
        res.json(user);
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.getUsers = getUsers;
const getUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    const { id } = req.params;
    const user = yield userMongo_1.default.findById(id);
    try {
        if (!user) {
            //throw new Error(`The user with ID: ${num_doc} is not already registered`);
            return res.status(400).json({
                msg: `The user with ID: ${id} is not already registered`
            });
        }
        res.json({ user });
    }
    catch (error) {
        //next(error);
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.getUser = getUser;
const postUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    try {
        const validate = yield userMongo_1.default.findOne(body, {
            where: {
                num_doc: body.num_doc
            }
        });
        if (validate) {
            return res.json({
                msg: `The user ${body.name} with num_doc: ${body.num_doc} is already exist`
            });
        }
        const user = new userMongo_1.default(body);
        yield user.save();
        res.json({
            msg: `The user ${body.name} has been registered successfully`,
            user
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.postUser = postUser;
const putUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    const { id } = req.params;
    const user = yield userMongo_1.default.findOne(body, {
        where: {
            _id: id
        }
    });
    try {
        if (!user) {
            //throw new Error(`The user with ID: ${num_doc} is not already registered`);
            return res.status(400).json({
                msg: `The user with ID: ${body.name} ${body.last_name} is not already registered`
            });
        }
        yield user.updateOne(body);
        res.json({
            msg: `The user ${body.name} ${body.last_name} it has updated successfully`,
            user
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.putUser = putUser;
const deleteUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    const { id } = req.params;
    try {
        const validate = yield userMongo_1.default.findOne(body, {
            where: {
                num_doc: body.num_doc
            }
        });
        if (!validate) {
            return res.json({
                msg: `The user ${body.name} with num_doc: ${body.num_doc} is not already exist`
            });
        }
        yield userMongo_1.default.deleteOne(body);
        res.json({
            msg: `The user ${body.name} has been deleted`
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.deleteUser = deleteUser;
//# sourceMappingURL=userMongo.js.map