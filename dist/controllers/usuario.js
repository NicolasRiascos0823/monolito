"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUser = exports.putUser = exports.postUser = exports.getUser = exports.getUsers = void 0;
const user_1 = __importDefault(require("../models/user"));
const getUsers = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const users = yield user_1.default.findAll();
    res.json({ users });
});
exports.getUsers = getUsers;
const getUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    /* res.json({
        msg:'getUser'
    }) */
    const user = yield user_1.default.findOne({
        where: { id }
    });
    try {
        if (!user) {
            return res.status(400).json({
                msg: `The user with ID: ${id} not already exist`
            });
        }
        res.json({ user });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.getUser = getUser;
const postUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    /* res.json({
        msg:'postUser',
        body
    }) */
    try {
        const existId = yield user_1.default.findOne({
            where: {
                id: body.id
            }
        });
        if (existId) {
            return res.status(400).json({
                msg: 'The user is already exist with ID: ' + body.id + " name: " + body.name
            });
        }
        const user = new user_1.default(body);
        yield user.save();
        res.json({
            msg: `The user ${body.name} has been registered successfully`,
            user
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.postUser = postUser;
const putUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    const { id } = req.params;
    /* res.json({
        msg:'putUser',
        body,
        id
    }) */
    try {
        const user = yield user_1.default.findByPk(id);
        if (!user) {
            return res.status(404).json({
                msg: "The user not already exist ID: " + id
            });
        }
        yield user.update(body);
        res.json({
            msg: `The user ${body.name} it has updated successfully`,
            user
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'You have a error in the data'
        });
    }
});
exports.putUser = putUser;
const deleteUser = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    /* res.json({
        msg:'deleteUser',
        id
    }) */
    const user = yield user_1.default.findByPk(id);
    if (!user) {
        return res.status(404).json({
            msg: "The user not already exist ID: " + id
        });
    }
    //logic delete
    //await user.update({status:false});
    //physic delete
    yield user.destroy();
    res.json({
        msg: `The user with ID:${id}  has been deleted`
    });
});
exports.deleteUser = deleteUser;
//# sourceMappingURL=usuario.js.map